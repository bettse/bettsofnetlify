# bettsofnetlify

This is a place where ~the Netlify team~ Eric (Betts) posts pictures of ~their pets~himself.

## A little note about this website

This is a loving tribute (and fork) of https://github.com/netlify/petsofnetlify

## Running locally

Want to tinker with the code?

```sh

# clone the repo
git clone git@gitlab.com:netlify/bettsofnetlify.git

# move into the project and install dependencies
cd bettsofnetlify
npm install

# generate the site and watch for changes
npm start
```
